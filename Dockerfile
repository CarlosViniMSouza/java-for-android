FROM openjdk:18

COPY /src/App.java /app/App.java

WORKDIR /app

CMD ["java", "App.java"]
