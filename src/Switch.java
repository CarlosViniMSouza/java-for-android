public class Switch {
    public enum daysWeek {
        Sun, Mon, Tue
    }
    // Wed, Thu, Fri, Sat

    public static void main(String[] args) {
        daysWeek[] currentDay = daysWeek.values();

        for (daysWeek day : currentDay) {
            switch (day) {
                case Sun, Mon, Tue:
                    System.out.println("Today is " + day);
                    break;
            }
        }

        int countNumber = 10;

        for (int i = 1; i <= countNumber; i++) {
            System.out.println(i);
        }

        for (int i = 1; i <= 3; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print("x");
            }

            System.out.println();
        }
    }
}
