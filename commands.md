1. Create container with Dockerfile: `docker build -t java-docker . -no-cache`

2. Run the container: `docker run java-docker`

``` 
Formule: IMC = weight / (height x height)

IMC - Body Mass Index (pt-br)

x < 16.0            -> low weight (level 1)
16.0 <= x <= 16.99  -> low weight (level 2)
17.0 <= x <= 18.49  -> low weight (level 3)
18.5 <= x <= 24.99  -> appropriate weight
25.0 <= x <= 29.99  -> overweight
30.0 <= x           -> obesity
```
