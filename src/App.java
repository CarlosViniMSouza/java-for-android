import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int num, currentYear;

        System.out.println("Insert your number: ");
        num = scanner.nextInt();

        String result = (num % 2 == 0) ? "even number" : "odd number";
        System.out.println("The number is: " + result);

        System.out.println("Insert current year: ");
        currentYear = scanner.nextInt();

        if (((currentYear % 4 == 0) && (currentYear % 100 != 0)) || (currentYear % 400 == 0)) {
            System.out.println("Current year is Leap!");
        } else {
            System.out.println("Current year is common!");
        }

        scanner.close();
    }
}
